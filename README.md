# Git Flow

Git Flow Development

## Rules
- Two branches, `master` and `dev`.
- At least two environments (*Development*, *Production*, and maybe *Staging <a.k.a. UAT>*)
- Always create a new branch from `master` and merge it back to `dev`
- One issue, one *active* branch.
- One issue, (can has) multiple MRs.
- **NO** Rebase
- Use *reset* instead of *revert*, if possible.
- Avoid using *cherry-pick*, use *merge* instead.
- If `master` update, `dev` has to either.

## Deployment Detail
| Environment | Watching | Event |
| ----------- | -------- | ----- |
| Development | `dev` branch | Push |
| Staging | `master` branch | Push |
| Production | Any version tag (ex. `v1.0`) | Push |

## Contribution Guide
[PlantUML](https://plantuml.com/) is a language we use to create flow graph here.

### Recommended Tools
- VS Code
- PlantUML extension for VS Code

### Generating UML
1.  Install `plantuml`
    ```bash
    $ brew install plantuml
    ```
2.  Generate UML
    ```bash
    $ cd git-flow
    $ plantuml {filename}
    ```

## Contributors
- @earthpyy